---
type: page
template: ""
title: Open Source Resources
metaTitle: Open Source Resources
metaDescription: ""
url: open-source-resources
lang: en
switchUrl: open-source-zdroje
inMenu: false
menuOrder: 9999
menuTitle: ""
isRedirected: false
isRedirectPermanent: false
redirectUrl: ""
index: "index"
follow: "follow"
activeFrom: ""
activeTo: ""
category: others
---

# Open Source

For over 20 years the Open Source Initiative (OSI) has worked to raise awareness and adoption of open source software, and build bridges between open source communities of practice. As a global non-profit, the OSI champions software freedom in society through education, collaboration, and infrastructure, stewarding the Open Source Definition (OSD), and preventing abuse of the ideals and ethos inherent to the open source movement.

Open source software is made by many people and distributed under an OSD-compliant license which grants all the rights to use, study, change, and share the software in modified and unmodified form. Software freedom is essential to enabling community development of open source software.

[[resource](https://opensource.org/)]

---

## Melodies

[[resource](http://allthemusic.info/)]

[[download](https://archive.org/download/allthemusicllc-datasets)]

---

## Free music

[[resource](https://wiki.pirati.cz/hudba/start)]

[[download](https://hudba.pirati.cz/archivy/)]

---

## Free images (640\*416)

[[resource](https://babelia.libraryofbabel.info/)]

---

## Texts

[[resource](https://libraryofbabel.info/)]
