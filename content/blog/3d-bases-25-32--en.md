---
type: page
template: blogPost
title: Round base modification - 25mm to 32mm
metaTitle: Round base modification - 25mm to 32mm
metaDescription: ""
url: round-base-modification-25mm-to-32mm
lang: en
switchUrl: modifikator-kulateho-podstavce-25mm-na-32mm
inMenu: false
menuOrder: 9999
menuTitle: ""
isRedirected: false
isRedirectPermanent: false
redirectUrl: ""
index: "index"
follow: "follow"
activeFrom: ""
activeTo: ""
category: 3d-print
---

# Round base modification - 25mm to 32mm

The popular game Warhammer 40 000 changed with the 9th edition.

As a part of these changes, you need to change bases for a lot of miniatures.

And you really do not want to remove your scenic and painted bases. So that's why are these adapters so useful.

You do not have to remove your nice bases and you will save some time.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Dy0qhKHrFho" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Adapters are available in sets of 40 pieces. You must cut them off.

![Round base modification - 25mm to 32mm - set top](../../static/cms/media/bases-25-to-32-top.jpg "shora")

![Round base modification - 25mm to 32mm - set bottom](../../static/cms/media/bases-25-to-32-bottom.jpg "zdola")

![Round base modification - 25mm to 32mm - before cutting](../../static/cms/media/bases-25-to-32-before.jpg "před začištěním")

![Round base modification - 25mm to 32mm - after cutting](../../static/cms/media/bases-25-to-32-after.jpg "po začištění")

If you wanna have your own set of adapters, [contact me](mailto:pilnaj.t@gmail.com).
