---
type: page
template: blog
title: Blog EN
metaTitle: "Blog"
metaDescription: ""
url: blog
lang: en
switchUrl: "blog"
inMenu: false
menuOrder: 4
menuTitle: Blog
isRedirected: false
isRedirectPermanent: false
redirectUrl: ""
index: "index"
follow: "follow"
activeFrom: ""
activeTo: ""
category: none
---

# Blog

You can find here everything I professionally love: web development (frontend, newsletter, banners, SEO) and 3D printing (3D models, prints, printer upgrades).
