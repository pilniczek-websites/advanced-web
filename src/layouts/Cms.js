import React from "react"
import { node } from "prop-types"
import StyledLayout from "./style"
import GlobalStyle from "../_css"

const Cms = ({ children }) => {
	return (
		<>
			<GlobalStyle />
			<StyledLayout id="app" className="app layout-index">
				<div className="app__body">
					{children}
				</div>
			</StyledLayout>
		</>
	)
}

Cms.propTypes = {
	children: node.isRequired,
}

export default Cms
