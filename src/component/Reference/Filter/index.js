import React, { useState } from "react"
import {  arrayOf } from "prop-types"
import {filter as propFilter} from "../../../_props"
import StyledFilter from "./style"

const Filter = ({ content }) => {
	const [filter, setFilter] = useState(content.content)
	return (
		<StyledFilter>
			<strong className="filter__headline">{content.title}</strong>
			{filter.map(item => {
				const { id, checked } = item
				return (
					<label htmlFor={id} className="filter__option" key={id}>
						<span className="filter__text">{id}</span>
						<input
							className="filter__input"
							type="checkbox"
							id={id}
							name={id}
							defaultChecked={checked}
							onChange={() => {
								const newFilter = filter.map(fItem => {
									return fItem.id === id ? { id, checked: !checked } : fItem
								})
								return setFilter(newFilter)
							}}
						/>
					</label>
				)
			})}
		</StyledFilter>
	)
}

Filter.propTypes = {
	content: arrayOf(propFilter).isRequired,
}

export default Filter
