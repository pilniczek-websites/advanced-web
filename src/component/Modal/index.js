import React from "react"
import Modal from "react-modal"
import Button from "../Link/Button"
import StyledModal from "./style"
import useGlobal from "../../store"
import { skin } from "../../_var"

const MyModal = ({ children }) => {
	const [globalState, globalActions] = useGlobal()

	return (
		<Modal
			ariaHideApp={false} // https://github.com/reactjs/react-modal/issues/576#issuecomment-349081325
			isOpen={globalState.modalOpen}
			onRequestClose={() => {
				globalActions.modalOpen(false)
			}}
			style={{
				content: {
					position: "relative",
					top: "0",
					bottom: "0",
					left: "0",
					right: "0",
					padding: "0",
					maxWidth: "600px",
					maxHeight: "100%",
				},
			}}
		>
			<StyledModal>
				<div className="modal__top">
					<Button
						href=""
						skin={skin.darkGhost}
						className="modal__close"
						onClick={() => {
							globalActions.modalOpen(false)
						}}
					>
						&times;
					</Button>
					<h2 className="modal__headline">Přihláška do ASWA</h2>
				</div>
				<div className="modal__content">{children}</div>
			</StyledModal>
		</Modal>
	)
}

export const ModalProps = {}

MyModal.propTypes = {
	...ModalProps,
}

export default MyModal
