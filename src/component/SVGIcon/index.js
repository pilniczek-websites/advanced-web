import React from "react"
import PropTypes from "prop-types"
import StyledSVG from "./style"
import iconPaths from "./iconPaths"

const SVGIcon = ({ name, ...rest }) => {
	return (
		<StyledSVG
			aria-hidden="true"
			focusable="false"
			data-prefix="fas"
			data-icon={name}
			role="img"
			xmlns="http://www.w3.org/2000/svg"
			viewBox={iconPaths[name].viewBox}
			{...rest}
		>
			{iconPaths[name].path}
		</StyledSVG>
	)
}

SVGIcon.propTypes = {
	name: PropTypes.oneOf(["arrowExternal"]).isRequired,
}

export default SVGIcon
