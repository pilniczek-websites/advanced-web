---
type: page
template: contact
title: Contact EN
metaTitle: "Contact"
metaDescription: "Developer of web applications and HTML emails."
url: contact
lang: en
switchUrl: "kontakt"
inMenu: true
menuOrder: 10
menuTitle: Contact
isRedirected: false
isRedirectPermanent: true
redirectUrl: ""
index: "index"
follow: "follow"
activeFrom: ""
activeTo: ""
category: none
---

# Contact

I'm Tom. I develop and maintain several websites.

The best way to contact me is an [e-mail](mailto:pilnaj.t@gmail.com). Feel free to write in English or in Czech.

You can follow me on any mentioned social network.
