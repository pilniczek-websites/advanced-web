const path = require("path")

const LANG_INFO = {
	// http://www.lingoes.net/en/translator/langcode.htm
	cs: {
		label: "česky",
		url: "",
		switchLang: "en",
	},
	en: {
		label: "english",
		url: "en",
		switchLang: "cs",
	},
}

const thisYear = new Date().getFullYear()
const ageOptions = ["recent", "recent", "aging"]
const dateToAge = year => {
	const age = ageOptions[thisYear - year] || "old"
	return age
}

exports.onCreateNode = ({ node, actions }) => {
	const { createNodeField } = actions
	if (node.internal.type === `WebsitesJson`) {
		createNodeField({
			node,
			name: "filterableProps",
			value: {
				...node.props,
				age: dateToAge(node.props.date),
			},
		})
	}
}

exports.createPages = ({ graphql, actions }) => {
	const { createPage, createRedirect } = actions
	const TEMPLATE_OPTION = {
		index: {
			path: path.resolve("./src/template/Index.js"),
		},
	}

	return new Promise((resolve, reject) => {
		resolve(
			graphql(
				`
					{
						allMarkdownRemark(
							filter: { frontmatter: { type: { eq: "page" } } }
						) {
							edges {
								node {
									frontmatter {
										type
										title
										metaTitle
										metaDescription
										url
										lang
										switchUrl
										template
										isRedirected
										isRedirectPermanent
										redirectUrl
										title
										index
										follow
										category
									}
									html
								}
							}
						}
					}
				`,
			).then(result => {
				if (result.errors) {
					console.log(result.errors); // eslint-disable-line
					reject(result.errors)
				}

				const pages = result.data.allMarkdownRemark.edges

				pages.forEach(({ node }) => {
					const {
						type,
						metaTitle,
						metaDescription,
						url,
						lang,
						switchUrl,
						template,
						isRedirected,
						isRedirectPermanent,
						redirectUrl,
						index,
						follow,
						category,
					} = node.frontmatter
					const { html } = node
					const fixSlash = string => (string.length === 0 ? "" : `/${string}`)
					const checkSlash = string =>
						string.length === 0 ? "/" : `${string}/`
					if (isRedirected) {
						createRedirect({
							fromPath: checkSlash(
								`${fixSlash(LANG_INFO[lang].url)}${fixSlash(url)}`,
							),
							toPath: checkSlash(
								`${fixSlash(LANG_INFO[lang].url)}${fixSlash(redirectUrl)}`,
							),
							isPermanent: isRedirectPermanent,
							redirectInBrowser: true,
						})
					} else {
						createPage({
							path: checkSlash(
								`${fixSlash(LANG_INFO[lang].url)}${fixSlash(url)}`,
							),
							component: TEMPLATE_OPTION.index.path,
							context: {
								type,
								metaTitle,
								metaDescription,
								url,
								lang,
								html,
								template,
								index,
								follow,
								switchUrl: {
									url: checkSlash(
										`${fixSlash(
											LANG_INFO[LANG_INFO[lang].switchLang].url,
										)}${fixSlash(switchUrl)}`,
									),
									label: `${LANG_INFO[LANG_INFO[lang].switchLang].label}`,
								},
								repo: template === "blog" ? `https://gitlab.com/pilniczek-websites/advanced-web/blob/master/content/blog/${url}--${lang}.md` : "https://gitlab.com/pilniczek-websites/advanced-web",
								category,
							},
						})
					}
				})
			}),
		)
	})
}

/* exports.onCreatePage = async ({ page, actions }) => {
	const { createPage, deletePage } = actions;
	// Check if the page is a localized 404
	if (page.path.match(/^\/[a-z]{2}\/404\/$/)) {
		const oldPage = { ...page };
		// Get the language code from the path, and match all paths
		// starting with this code (apart from other valid paths)
		const langCode = page.path.split(`/`)[1];
		page.matchPath = `/${langCode}/*`;
		page.context = PAGES_CONFIG.page404[langCode].context;
		// Recreate the modified page
		deletePage(oldPage);
		createPage(page);
	}
}; */
