import React, { Component } from "react"
import PropTypes from "prop-types"
import CookieConsent, { Cookies } from "react-cookie-consent"
import styled from "styled-components"
import { Link } from "gatsby"
import { useZIndex } from "../../_var"

const CookieConsentHack = styled.div`
	position: fixed;
	bottom: 0;
	left: 0;
	right: 0;
	z-index: ${useZIndex("footer")};
	.cookieConsent {
		position: static !important;
	}
	@media (max-width: 480px) {
		.cookieConsent {
			flex-direction: column;
			align-items: flex-end !important;
			& > div {
				flex: none !important;
			}
			& > button {
				margin: 1.5rem !important;
			}
		}
	}
`
class CookieBanner extends Component {
	constructor(props) {
		super(props)
		this._isMounted = false
		this.state = {
			cookieBannerHeight: 0,
		}
	}

	componentDidMount() {
		this._isMounted = true
		this.updateHeight()
		window.addEventListener("resize", this.updateHeight)
	}

	componentDidUpdate() {
		this.updateHeight()
	}

	componentWillUnmount() {
		window.removeEventListener("resize", this.updateHeight)
		this._isMounted = false
	}

	updateHeight = () => {
		const { cookieBannerHeight } = this.state
		if (
			typeof Cookies.get("gdpr") === "undefined" &&
			(cookieBannerHeight !== this.div.clientHeight || cookieBannerHeight === 0)
		) {
			this.setState(() => ({
				cookieBannerHeight: this.div.clientHeight,
			}))
		}
	};

	render() {
		const { lang } = this.props
		return (
			<CookieConsentHack
				ref={div => {
					this.div = div
				}}
			>
				{/* I need CookieConsent height for proper responsive behavior. */}
				{/* CookieConsent can not use "ref" to find it out. */}
				<CookieConsent
					location="bottom"
					buttonText="buttonText"
					cookieName="gdpr"
					style={{}}
					contentStyle={{ margin: "0" }}
					buttonStyle={{}}
					onAccept={() => {
						this.setState({
							cookieBannerHeight: 0,
						})
					}}
				>
					msg
					<Link className="cookie-link" to={lang}>
						cookie policy.
					</Link>
				</CookieConsent>
			</CookieConsentHack>
		)
	}
}

CookieBanner.propTypes = {
	lang: PropTypes.string.isRequired,
}

export default CookieBanner
