---
type: page
template: blogPost
title: Modifikátor kulatého podstavce 25mm na 32mm
metaTitle: Modifikátor kulatého podstavce 25mm na 32mm
metaDescription: ""
url: modifikator-kulateho-podstavce-25mm-na-32mm
lang: cs
switchUrl: round-base-modification-25mm-to-32mm
inMenu: false
menuOrder: 9999
menuTitle: ""
isRedirected: false
isRedirectPermanent: false
redirectUrl: ""
index: "index"
follow: "follow"
activeFrom: ""
activeTo: ""
category: 3d-print
---

# Modifikátor kulatého podstavce 25mm na 32mm

Populární hra Warhammer 40 000 dostala v nové edici novou sadu pravidel.

Jejich součástí je i změna velikosti podstavce pod mnoha různými modely.

Vzhledem k tomu, že vyměnit scénické podstavce na větších počtech modelů by bylo náročné, vyřešil jsem problém jednoduchým adaptérem.

Nemusíte tedy zahazovat své již vymodelované podstavce a ušetříte si tak spoustu práce.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Dy0qhKHrFho" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Modifikátory jsou k dostání v platíčcích po 40 kusech. Je potřeba je vyřezat a začistit,

![Modifikátor kulatého podstavce 25mm na 32mm - plato shora](../../static/cms/media/bases-25-to-32-top.jpg "shora")

![Modifikátor kulatého podstavce 25mm na 32mm- plato zdola](../../static/cms/media/bases-25-to-32-bottom.jpg "zdola")

![Modifikátor kulatého podstavce 25mm na 32mm - před začištěním](../../static/cms/media/bases-25-to-32-before.jpg "před začištěním")

![Modifikátor kulatého podstavce 25mm na 32mm - po začištění](../../static/cms/media/bases-25-to-32-after.jpg "po začištění")

Pokud máte o adaptéry zájem, [kontaktujte mne](mailto:pilnaj.t@gmail.com).
