module.exports = {
	pathPrefix: process.env.PATH_PREFIX || "/",
	siteMetadata: {
		author: `Tomas Pilnaj`,
		title: `Tomas Pilnaj`,
		description: `Vývojář webových aplikací a kodér HTML e-mailů.`,
		siteUrl: "https://tomas-pilnaj.cz",
	},
	plugins: [
		`gatsby-plugin-sitemap`,
		{
			resolve: "gatsby-plugin-robots-txt",
			options: {
				host: "https://tomas-pilnaj.cz",
				sitemap: "https://tomas-pilnaj.cz/sitemap.xml",
				policy: [{ userAgent: "*", allow: "/" }],
			},
		},
		{
			resolve: `gatsby-plugin-favicon`,
			options: {
				logo: "./src/image/favicon.png",
				icons: {
					android: true,
					appleIcon: true,
					appleStartup: true,
					coast: true,
					favicons: true,
					firefox: true,
					opengraph: true,
					twitter: true,
					yandex: true,
					windows: true,
				},
			},
		},
		{
			resolve: "gatsby-plugin-eslint",
			options: {
				exclude: /(node_modules|.cache|public)/,
				stages: ["develop"],
				options: {
					failOnError: true,
				},
			},
		},
		`gatsby-plugin-react-helmet`,
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `images`,
				path: `${__dirname}/static/cms/media`,
			},
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: "content",
				path: `${__dirname}/content/`,
			},
		},
		{
			resolve: `gatsby-transformer-sharp`,
			options: {
				quality: 85,
			},
		},
		`gatsby-plugin-force-trailing-slashes`,
		`gatsby-plugin-styled-components`,
		`gatsby-background-image`,
		{
			resolve: `gatsby-plugin-sharp`,
			options: {
				defaultQuality: 85,
			},
		},
		{
			resolve: "gatsby-plugin-firebase",
			options: {
				features: {
					database: true,
					firestore: true,
				},
				credentials: {
					apiKey: process.env.GATSBY_FIB_API_KEY,
					authDomain: process.env.GATSBY_FIB_AUTH_DOMAIN,
					databaseURL: process.env.GATSBY_FIB_DATABASE_URL,
					projectId: process.env.GATSBY_FIB_PROJECT_ID,
					storageBucket: process.env.GATSBY_FIB_STORAGE_BUCKET,
					messagingSenderId: process.env.GATSBY_FIB_MESSAGING_SENDER_ID,
					appId: process.env.GATSBY_FIB_APP_ID,
					measurementId: process.env.GATSBY_FIB_MEASUREMENT_ID,
				},
			},
		},
		// `gatsby-plugin-recaptcha`,
		{
			resolve: `gatsby-plugin-manifest`,
			options: {
				name: `gatsby-starter-default`,
				short_name: `starter`,
				start_url: `/`,
				background_color: `#663399`,
				theme_color: `#663399`,
				display: `minimal-ui`,
				icon: "./src/image/favicon.png", // This path is relative to the root of the site.
			},
		},
		{
			resolve: "gatsby-plugin-google-tagmanager",
			options: {
				id: "GTM-M684KB",
				includeInDevelopment: false,
			},
		},
		`gatsby-plugin-extract-schema`,
		`gatsby-transformer-json`,
		{
			resolve: `gatsby-transformer-remark`,
			options: {
				plugins: [
					`gatsby-remark-unwrap-images`,
					{
						resolve: `gatsby-remark-relative-images`, // must be before gatsby-remark-images
					},
					{
						resolve: `gatsby-remark-images`,
						options: {
							maxWidth: 800,
							backgroundColor: "transparent",
							linkImagesToOriginal: false,
							showCaptions: true,
						},
					},
					{
						resolve: "gatsby-remark-embed-gist",
						options: {
							includeDefaultCss: false,
						},
					},
					`gatsby-plugin-sharp`,
					{
						resolve: `gatsby-remark-responsive-iframe`,
					},
					`gatsby-remark-prismjs`,
					`gatsby-remark-copy-linked-files`,
				],
			},
		},
		{
			resolve: `gatsby-plugin-layout`,
			options: {
				component: require.resolve(`./src/template/Index.js`),
			},
		},
		/* { POSSIBLE solution
			resolve: `gatsby-plugin-google-fonts`,
			options: {
				fonts: [
					`Oswald:300,500&subset=latin-ext`,
				],
				display: 'swap',
			},
		}, */
		{
			resolve: `gatsby-plugin-netlify-cms`,
			options: {
				enableIdentityWidget: false,
				modulePath: `${__dirname}/cms/cms.js`,
				manualInit: true,
			},
		},
		{
			resolve: "gatsby-plugin-sentry",
			options: {
				dsn: process.env.GATSBY_SENTRY_DSN_URL,
				environment: process.env.GATSBY_ENV,
				enabled:
					process.env.GATSBY_ENV === "production" ||
					process.env.GATSBY_ENV === "dev",
			},
		},
		{
			resolve: `gatsby-plugin-remove-serviceworker`,
		},
	],
}
