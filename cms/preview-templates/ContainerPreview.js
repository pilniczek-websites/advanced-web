import React from "react"
import { func, shape } from "prop-types"
import { markdownToHtml } from "netlify-cms-widget-markdown/dist/esm/serializers/index"
import { StyleSheetManager } from "styled-components"
import HTMLContent from "../../src/component/Content/HTMLContent"
import Cms from "../../src/layouts/Cms"

const ContainerPreview = ({ entry, getAsset }) => {
	const data = entry.getIn(["data"])
	const html = markdownToHtml(data.getIn(["body"]), getAsset)

	/*
	 *	https://github.com/netlify/netlify-cms/issues/793#issuecomment-373723829
	 *	must pass styles to preview iframe
	 */
	const iframe = document.getElementsByTagName("iframe")[0]
	const iframeHeadElem = iframe.contentDocument.head

	return (
		<StyleSheetManager target={iframeHeadElem}>
			<Cms>
				<HTMLContent innerHTML={html} />
			</Cms>
		</StyleSheetManager>
	)
}

ContainerPreview.propTypes = {
	entry: shape({
		getIn: func.isRequired,
	}).isRequired,
	getAsset: func.isRequired,
}
export default ContainerPreview
