import cms from "../cmsContent.json"

const page = [
	{
		label: cms.page.template,
		name: "template",
		widget: "select",
		allow_add: false,
		options: ["blog", "index", "contact", "reference"],
		default: ["blog"],
	},
	{
		label: cms.page.title,
		name: "title",
		widget: "string",
	},
	{
		label: cms.page.metaTitle,
		name: "metaTitle",
		widget: "string",
	},
	{
		label: cms.page.metaDescription,
		name: "metaDescription",
		widget: "string",
	},
	{
		label: cms.page.url,
		name: "url",
		widget: "string",
	},
	{
		label: cms.page.lang,
		name: "lang",
		widget: "select",
		options: ["cs", "en"],
	},
	{
		label: cms.page.switchUrl,
		name: "switchUrl",
		widget: "string",
	},
	{
		label: cms.page.inMenu,
		name: "inMenu",
		widget: "boolean",
		default: "false",
	},
	{
		label: cms.page.menuOrder,
		name: "menuOrder",
		widget: "number",
		default: 9999,
	},
	{
		label: cms.page.menuTitle,
		name: "menuTitle",
		widget: "string",
		default: "",
	},
	{
		label: cms.page.isRedirected,
		name: "isRedirected",
		widget: "boolean",
		default: false,
	},
	{
		label: cms.page.isRedirectPermanent,
		name: "isRedirectPermanent",
		widget: "boolean",
		default: false,
	},
	{
		label: cms.page.redirectUrl,
		name: "redirectUrl",
		widget: "string",
		default: "",
	},
	{
		label: cms.page.index,
		name: "index",
		widget: "select",
		allow_add: false,
		options: ["index", "noindex"],
		default: ["index"],
	},
	{
		label: cms.page.follow,
		name: "follow",
		widget: "select",
		allow_add: false,
		options: ["follow", "nofollow"],
		default: ["index"],
	},
	{
		label: cms.page.activeFrom,
		name: "activeFrom",
		widget: "date",
		default: "",
	},
	{
		label: cms.page.activeTo,
		name: "activeTo",
		widget: "date",
		default: "",
	},
	{
		label: cms.page.body,
		name: "body",
		widget: "markdown",
	},
	{
		label: cms.page.category,
		name: "follow",
		widget: "select",
		allow_add: false,
		options: ["others", "frontend", "3d", "security"],
		default: ["others"],
	},
]

export default page