import React from "react"
import { func, shape } from "prop-types"
import { StyleSheetManager } from "styled-components"
import ContactForm from "../../src/component/ContactForm"
import Cms from "../../src/layouts/Cms"

const FormPreview = ({ entry }) => {
	const data = entry.getIn(["data"])
	const setup = {
		email: data.getIn(["email"]).toJS(),
		message: data.getIn(["message"]).toJS(),
		submit: data.getIn(["submit"]).toJS(),
		renderError: data.getIn(["renderError"]).toJS(),
	}

	/*
	 *	https://github.com/netlify/netlify-cms/issues/793#issuecomment-373723829
	 *	must pass styles to preview iframe
	 */
	const iframe = document.getElementsByTagName("iframe")[0]
	const iframeHeadElem = iframe.contentDocument.head

	return (
		<StyleSheetManager target={iframeHeadElem}>
			<Cms>
				<ContactForm setup={setup} cms />
			</Cms>
		</StyleSheetManager>
	)
}

FormPreview.propTypes = {
	entry: shape({
		getIn: func.isRequired,
	}).isRequired,
}
export default FormPreview
