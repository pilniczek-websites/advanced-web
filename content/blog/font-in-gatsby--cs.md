---
type: page
template: blogPost
title: Font v GatsbyJS
metaTitle: Font v GatsbyJS
metaDescription: ""
url: font-v-gatsby
lang: cs
switchUrl: ""
inMenu: false
menuOrder: 9999
menuTitle: ""
isRedirected: false
isRedirectPermanent: false
redirectUrl: ""
index: "index"
follow: "follow"
activeFrom: ""
activeTo: ""
category: frontend
---

# Font v GatsbyJS

Používání fontu vypadá díky pluginům velmi snadno i pro méně zkušeného frontenďáka. Nicméně testování tří nejpoužívanějších pluginů (pomocí lighthouse) ukáže, že není plugin jako plugin. Navíc nemusíme nezbytně sáhnout po pluginu, můžeme fonty použít i "ručně".

## Vybereme google font

[**Oswald**](https://fonts.google.com/specimen/Oswald)

```
<link href="https://fonts.googleapis.com/css?family=Oswald:300,500&display=swap&subset=latin-ext" rel="stylesheet">
```

```
@import url('https://fonts.googleapis.com/css?family=Oswald:300,500&display=swap');
```

### Chceme opravdu toto nastavení?

Google font customizátor nenabídne možnost konfigurace vlastnosti `font-display` a rovnou nastaví hodnotu `swap`. Nicméně si můžete hodnotu v URL přepsat, třeba na [`fallback`](https://www.vzhurudolu.cz/prirucka/css-font-display#fallback), funguje to bez problému.

## Implementace

### 1. Pluginy

#### 1.1 gatsby-plugin-web-font-loader (52.9k)

Plugin: https://www.gatsbyjs.org/packages/gatsby-plugin-web-font-loader/

Repozitář: https://github.com/escaladesports/legacy-gatsby-plugin-web-font-loader

Repozitář není udržován.

Lighthouse report vrací preconnect warning.

`gist:pilniczek/9213f5ee79c6473b97ffb3e928950fff`

#### 1.2 gatsby-plugin-prefetch-google-fonts (43.7k)

Plugin: https://www.gatsbyjs.org/packages/gatsby-plugin-prefetch-google-fonts/?=font

Repozitář: https://github.com/escaladesports/legacy-gatsby-plugin-prefetch-google-fonts

Repozitář není udržován. Nebyl jsem schopen spustit úspěšný build - což je problém nejednoho dalšího člověka. Více informací na [GitHubu](https://github.com/escaladesports/legacy-gatsby-plugin-prefetch-google-fonts/issues/2).

`gist:pilniczek/148deb14400b358b7a2c31cc26b0cd22`

#### 1.3 gatsby-plugin-google-fonts (21.8k)

Plugin: https://www.gatsbyjs.org/packages/gatsby-plugin-google-fonts/?=font

Repozitář: https://github.com/didierfranc/gatsby-plugin-google-fonts

Žádný lighthouse warning. Můžete ho s klidem použít.

`gist:pilniczek/f7d1b49855933c53ed5cd988cefd07e7`

### 2. Zdroje z Google Fonts

#### 2.1 Použití tagu v SEO komponentě

Nebo pomocí react-helmet. Tato implementace je jednoduchá. Zvlášť když už nějakou SEO komponentu máte, nebo používáte react-helmet.

Podle Lighthouse reportu je vše v pořádku.

#### 2.2 Použití CSS importem

CSS Import v souboru globálích CSS definic je v pořádku.

Žádný lighthouse warning.

### 3. Vlastní CSS Definice

Pokud chcete použít nějaké neortodoxní definice nebo máte rádi nad kódem úplnou kontrolu, není problém CSS definice zkopírovat z Googlu.

`gist:pilniczek/3c5dc0a42d5d9766bf56968fb7242c20`

Ale pozor, když zdroj neupravíte, Lighthouse vám zase vrátí preconnect warning. Stáhněte tedy font a upravte cesty.

`gist:pilniczek/48f97f5306373765d6064cbc6ce41f67`

## Závěr

Nevyplácí se předpokládat, že nejpoužívanější nebo nejoblíbenější řešení jsou nejlepší a nejudržovanější. Ideální nástroj jsem našel až jako třetí v pořadí.

Použil bych rozhodně [gatsby-plugin-google-fonts](https://www.gatsbyjs.org/packages/gatsby-plugin-google-fonts/) pro externí fonty. Nebál bych se ale ani fonty stáhnout a použít CSS z posledního příkladu.

## Resources

[Oswald font](https://fonts.google.com/specimen/Oswald) na Google fontech

[`font-display: fallback`](https://www.vzhurudolu.cz/prirucka/css-font-display#fallback) na Vzhůru dolů

[Lighthouse](https://developers.google.com/web/tools/lighthouse) testovací nástroj
