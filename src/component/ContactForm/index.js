import React, { useState } from "react"
import { Formik } from "formik"
import { bool, object } from "prop-types"
import { useFirebase } from "gatsby-plugin-firebase"
import Form from "../Form"
import ErrorBoundary from "../ErrorBoundary"

const Contact = ({ data, cms }) => {
	const { form } = data

	const [firebase, setFirebase] = useState(null)

	useFirebase(fib => {
		setFirebase(fib)
	}, [])

	return (
		<ErrorBoundary
			errorHeadline={form.renderError.errorHeadline}
			errorParagraph={form.renderError.errorParagraph}
		>
			<Formik
				initialValues={{/*
					email: form.email.initialValue,
					message: form.message.initialValue,
				*/}}
				onSubmit={async (values, { setSubmitting, resetForm, setStatus }) => {
					if (/* values.recaptcha && */ !cms) {
						try {
							await firebase
								.firestore()
								.collection(`FormData`)
								.add(values)
							resetForm({})
							setSubmitting(false)
							setStatus("success")
						} catch (e) {
							setStatus("error")
								console.log(e); // eslint-disable-line
							throw e
						}
					} else {
						setStatus("reCaptchaError")
					}
				}}
			>
				{({ isSubmitting, status, values, errors, touched, setFieldValue }) => {
					return (
						<Form
							id="logInForm"
							isSubmitting={isSubmitting}
							status={status}
							values={values}
							errors={errors}
							touched={touched}
							setFieldValue={setFieldValue}
							reCaptchaVisible={!cms}
							data={form}
							cms={cms}
						/>
					)
				}}
			</Formik>
		</ErrorBoundary>
	)
}

Contact.propTypes = {
	data: object.isRequired, // eslint-disable-line
	cms: bool,
}

Contact.defaultProps = {
	cms: false,
}

export default Contact
