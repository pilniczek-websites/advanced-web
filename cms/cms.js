import CMS from "netlify-cms-app"
// import cms from "./cmsContent.json"
import ContainerPreview from "./preview-templates/ContainerPreview"
import FormPreview from "./preview-templates/FormPreview"
import "./style.css"
// import page from "./collections/page"

CMS.init({
	config: {
		backend: {
			name: "gitlab",
			branch: "master",
			repo: "pilniczek-websites/advanced-web",
			auth_type: "implicit",
			app_id: process.env.GATSBY_APP_ID || "",
		},
		media_folder: "static/cms/media",
		public_folder: "/../../../../cms/media",
		collections: [
			{
				name: "page",
				label: "Page",
				folder: "content/page",
				create: true,
				extension: "md",
				slug: "{{url}}--{{lang}}",
				fields: [
					{
						label: "template",
						name: "template",
						widget: "select",
						allow_add: false,
						options: ["blog", "index", "contact", "reference"],
						default: ["blog"],
					},
					{
						label: "title",
						name: "title",
						widget: "string",
					},
					{
						label: "metaTitle",
						name: "metaTitle",
						widget: "string",
					},
					{
						label: "metaDescription",
						name: "metaDescription",
						widget: "string",
					},
					{
						label: "url",
						name: "url",
						widget: "string",
					},
					{
						label: "lang",
						name: "lang",
						widget: "select",
						options: ["cs", "en"],
					},
					{
						label: "switchUrl",
						name: "switchUrl",
						widget: "string",
					},
					{
						label: "inMenu",
						name: "inMenu",
						widget: "boolean",
						default: "false",
					},
					{
						label: "menuOrder",
						name: "menuOrder",
						widget: "number",
						default: 9999,
					},
					{
						label: "menuTitle",
						name: "menuTitle",
						widget: "string",
						default: "",
					},
					{
						label: "isRedirected",
						name: "isRedirected",
						widget: "boolean",
						default: false,
					},
					{
						label: "isRedirectPermanent",
						name: "isRedirectPermanent",
						widget: "boolean",
						default: false,
					},
					{
						label: "redirectUrl",
						name: "redirectUrl",
						widget: "string",
						default: "",
					},
					{
						label: "index",
						name: "index",
						widget: "select",
						allow_add: false,
						options: ["index", "noindex"],
						default: ["index"],
					},
					{
						label: "follow",
						name: "follow",
						widget: "select",
						allow_add: false,
						options: ["follow", "nofollow"],
						default: ["index"],
					},
					{
						label: "activeFrom",
						name: "activeFrom",
						widget: "date",
						default: "",
					},
					{
						label: "activeTo",
						name: "activeTo",
						widget: "date",
						default: "",
					},
					{
						label: "body",
						name: "body",
						widget: "markdown",
					},
					{
						label: "category",
						name: "follow",
						widget: "select",
						allow_add: false,
						options: ["others", "frontend", "3d", "security"],
						default: ["others"],
					},
				],
			},
			{
				name: "blog",
				label: "Blog",
				folder: "content/blog",
				create: true,
				extension: "md",
				slug: "{{url}}--{{lang}}",
				fields: [
					{
						label: "template",
						name: "template",
						widget: "select",
						allow_add: false,
						options: ["blog", "index", "contact", "reference"],
						default: ["blog"],
					},
					{
						label: "title",
						name: "title",
						widget: "string",
					},
					{
						label: "metaTitle",
						name: "metaTitle",
						widget: "string",
					},
					{
						label: "metaDescription",
						name: "metaDescription",
						widget: "string",
					},
					{
						label: "url",
						name: "url",
						widget: "string",
					},
					{
						label: "lang",
						name: "lang",
						widget: "select",
						options: ["cs", "en"],
					},
					{
						label: "switchUrl",
						name: "switchUrl",
						widget: "string",
					},
					{
						label: "inMenu",
						name: "inMenu",
						widget: "boolean",
						default: "false",
					},
					{
						label: "menuOrder",
						name: "menuOrder",
						widget: "number",
						default: 9999,
					},
					{
						label: "menuTitle",
						name: "menuTitle",
						widget: "string",
						default: "",
					},
					{
						label: "isRedirected",
						name: "isRedirected",
						widget: "boolean",
						default: false,
					},
					{
						label: "isRedirectPermanent",
						name: "isRedirectPermanent",
						widget: "boolean",
						default: false,
					},
					{
						label: "redirectUrl",
						name: "redirectUrl",
						widget: "string",
						default: "",
					},
					{
						label: "index",
						name: "index",
						widget: "select",
						allow_add: false,
						options: ["index", "noindex"],
						default: ["index"],
					},
					{
						label: "follow",
						name: "follow",
						widget: "select",
						allow_add: false,
						options: ["follow", "nofollow"],
						default: ["index"],
					},
					{
						label: "activeFrom",
						name: "activeFrom",
						widget: "date",
						default: "",
					},
					{
						label: "activeTo",
						name: "activeTo",
						widget: "date",
						default: "",
					},
					{
						label: "body",
						name: "body",
						widget: "markdown",
					},
					{
						label: "category",
						name: "follow",
						widget: "select",
						allow_add: false,
						options: ["others", "frontend", "3d", "security"],
						default: ["others"],
					},
				],
			},
			{
				name: "fragment",
				label: "Fragment",
				folder: "content/fragment",
				create: false,
				extension: "md",
				fields: [
					{
						label: "title",
						name: "title",
						widget: "string",
					},
					{
						label: "title",
						name: "type",
						widget: "string",
					},
					{
						label: "lang",
						name: "lang",
						widget: "string",
					},
					{
						label: "body",
						name: "body",
						widget: "markdown",
					},
				],
			},
			{
				name: "form",
				label: "labelForm",
				folder: "content/form",
				extension: "json",
				create: false,
				fields: [
					{
						label: "Titulek",
						name: "title",
						widget: "string",
					},
					{
						label: "Jazyk",
						name: "lang",
						widget: "string",
					},
					{
						label: "Systémový identifikátor",
						name: "id",
						widget: "string",
					},
					{
						label: "Nadpis",
						name: "headline",
						widget: "string",
					},
					{
						label: "Obsah formuláře",
						name: "form",
						widget: "object",
						fields: [
							{
								label: "Fieldset",
								name: "fieldset",
								widget: "list",
								allow_add: true,
								fields: [
									{
										label: "Podnadpis",
										name: "headline",
										widget: "string",
									},
									{
										label: "Systémový identigikátor",
										name: "id",
										widget: "string",
									},
									{
										label: "Pole formuláře",
										name: "input",
										widget: "list",
										allow_add: true,
										fields: [
											{
												label: "Message",
												name: "message",
												widget: "object",
												fields: [
													{
														label: "field",
														name: "field",
														widget: "string",
													},
													{
														label: "options",
														name: "options",
														widget: "list",
														allow_add: true,
														fields: [
															{
																label: "Label",
																name: "label",
																widget: "string",
															},
															{
																label: "Value",
																name: "value",
																widget: "string",
															},
															{
																label: "Disabled",
																name: "disabled",
																widget: "boolean",
															},
														],
													},
													{
														label: "type",
														name: "type",
														widget: "string",
													},
													{
														label: "required",
														name: "required",
														widget: "boolean",
													},
													{
														label: "validation",
														name: "validation",
														widget: "string",
													},
													{
														label: "initialValue",
														name: "initialValue",
														widget: "string",
													},
													{
														label: "name",
														name: "name",
														widget: "string",
													},
													{
														label: "labelInput",
														name: "label",
														widget: "string",
													},
													{
														label: "placeholder",
														name: "placeholder",
														widget: "string",
													},
													{
														label: "errorRequired",
														name: "requiredMessage",
														widget: "string",
													},
													{
														label: "errorValidation",
														name: "validationMessage",
														widget: "string",
													},
													{
														label: "value",
														name: "value",
														widget: "string",
													},
													{
														label: "visualStyle",
														name: "visualStyle",
														widget: "string",
													},
												],
											},
										],
									},
								],
							},
							{
								label: "submit.label",
								name: "submit",
								widget: "object",
								fields: [
									{
										label: "submit.submitLabel",
										name: "label",
										widget: "string",
									},
									{
										label: "submit.success",
										name: "success",
										widget: "string",
									},
									{
										label: "submit.error",
										name: "error",
										widget: "string",
									},
									{
										label: "submit.submitting",
										name: "submitting",
										widget: "string",
									},
									{
										label: "submit.reCaptchaError",
										name: "reCaptchaError",
										widget: "string",
									},
								],
							},
							{
								label: "renderError.label",
								name: "renderError",
								widget: "object",
								fields: [
									{
										label: "renderError.errorHeadline",
										name: "errorHeadline",
										widget: "string",
									},
									{
										label: "renderError.errorParagraph",
										name: "errorParagraph",
										widget: "string",
									},
								],
							},
						],
					},
				],
			},
		],
	},
})

CMS.registerPreviewTemplate("blog", ContainerPreview)
CMS.registerPreviewTemplate("page", ContainerPreview)
CMS.registerPreviewTemplate("fragment", ContainerPreview)
CMS.registerPreviewTemplate("form", FormPreview)
