---
type: page
template: index
title: "index EN"
metaTitle: "Tomas Pilnaj"
metaDescription: "Developer of web applications and HTML emails."
url: ""
lang: en
switchUrl: ""
inMenu: true
menuOrder: 1
menuTitle: Home
isRedirected: false
isRedirectPermanent: true
redirectUrl: ""
index: "index"
follow: "follow"
activeFrom: ""
activeTo: ""
category: none
---

# Tomáš Pilnaj

**Web solutions for your business**
