import styled from "styled-components"

const StyledErrorBoundary = styled.div`
	border: 1px solid #cc1d21;
	padding: 1.5rem;
	width: 100%;
`

export default StyledErrorBoundary
