import React, { Component } from "react"
import { element, string } from "prop-types"
import StyledErrorBoundary from "./style"

export default class ErrorBoundary extends Component {
	constructor(props) {
		super(props)
		this.state = { error: null }
	}

	componentDidCatch(error, errorInfo) {
		this.setState({ error })
		Sentry.configureScope(scope => {
			Object.keys(errorInfo).forEach(key => {
				scope.setExtra(key, errorInfo[key])
			})
		})
		Sentry.captureException(error)
	}

	render() {
		const { error } = this.state
		const { children, errorHeadline, errorParagraph } = this.props
		if (error) {
			// render fallback UI
			return (
				<StyledErrorBoundary>
					{errorHeadline !== "" && (
						<h2>
							{errorHeadline}
						</h2>
					)}
					{errorParagraph !== "" && (
						<p>
							{errorParagraph}
						</p>
					)}
				</StyledErrorBoundary>
			)
		}
		// when there's not an error, render children untouched
		return children
	}
}

ErrorBoundary.propTypes = {
	children: element.isRequired,
	errorHeadline: string,
	errorParagraph: string,
}

ErrorBoundary.defaultProps = {
	errorHeadline: "",
	errorParagraph: "",
}
