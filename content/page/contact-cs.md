---
type: page
template: contact
title: Kontakt CS
metaTitle: "Kontakt"
metaDescription: Vývojář webových aplikací a kodér HTML e-mailů.
url: kontakt
lang: cs
switchUrl: "contact"
inMenu: true
menuOrder: 10
menuTitle: Kontakt
isRedirected: false
isRedirectPermanent: true
redirectUrl: ""
index: "index"
follow: "follow"
activeFrom: ""
activeTo: ""
category: none
---

# Kontakt

Jsem Tomáš, vyvíjím a spravuji několik webových prezentací.

Zdaleka nejspolehlivějším kontaktem na mě je [e-mail](mailto:pilnaj.t@gmail.com).

Můžete se mnou také navázat kontakt prostřednictvím některé sociální sítě.
