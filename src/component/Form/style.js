import styled from "styled-components"
import { breakpoint } from "../../_var"

const StyledContact = styled.div`
	width: 100%;
	padding-bottom: 4rem;
	.form-input {
		display: block;
		min-width: 50%;
		@media (max-width: ${breakpoint.layout.to}px) {
			width: 100%;
		}
	}
	.form-item {
		display: flex;
		flex-direction: column;
		padding-bottom: 1rem;
	}
	.form-item__inner-row {
		display: flex;
		align-items: flex-start;
		justify-content: space-between;
		@media (max-width: ${breakpoint.layout.to}px) {
			display: block;
		}
	}
	.form-field-error {
		width: 100%;
		padding: 0.5rem 1.5rem 0.5rem 1.5rem;
		margin-left: 1.5rem;
		background-color: #cc1d21;
		color: white;
		border: 1px solid #272727;
		@media (max-width: ${breakpoint.layout.to}px) {
			margin: 0;
		}
	}
	.form-error {
		width: 100%;
		padding: 0.5rem 1.5rem 0.5rem 1.5rem;
		background-color: #cc1d21;
		color: white;
	}

	.form-success {
		width: 100%;
		padding: 0.5rem 1.5rem 0.5rem 1.5rem;
		background-color: #1fa618;
		color: white;
	}

	.form-submitting {
		width: 100%;
		padding: 0.5rem 1.5rem 0.5rem 1.5rem;
		background-color: #4d81a6;
		color: white;
	}
`

export default StyledContact
