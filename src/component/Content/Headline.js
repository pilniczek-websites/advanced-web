import React from "react"
import { string } from "prop-types"
import styled from "styled-components"
import { sanitized } from "../../_utils"

const StyledHeadline = styled.h1`
	line-height: 1.2;
	padding-bottom: 1.5rem !important;
	margin-top: 1rem;

	@media print {
		text-shadow: -1px -1px 0 rgba(0, 0, 0, 0.3);
	}
`

const Headline = ({ title }) => (
	<StyledHeadline
		className="page-header"
		dangerouslySetInnerHTML={{ __html: sanitized(title) }}
	/>
)

Headline.propTypes = {
	title: string.isRequired,
}

export default Headline
