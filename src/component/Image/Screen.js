import React from "react"
import styled from "styled-components"
import { oneOfType, string, shape, number } from "prop-types"
import Image from "./index"

const StyledScreen = styled.div`
	position: relative;
	/*overflow: hidden;*/
	padding: 2%;
	&:after {
		content: "";
		display: block;
		position: absolute;
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		background-image: url("/screen.svg");
    background-size: 100% 100%;
	}
`

const Screen = ({ data, alt, ...rest }) => (
	<StyledScreen {...rest}>
		<Image data={data} alt={alt} />
	</StyledScreen>
)

Screen.propTypes = {
	data: oneOfType([
		string,
		shape({
			childImageSharp: oneOfType([
				shape({
					fluid: shape({
						aspectRatio: number,
						sizes: string,
						src: string,
						srcSet: string,
						srcSetWebp: string,
						srcWebp: string,
					}),
				}),
				shape({
					fixed: shape({
						aspectRatio: number,
						sizes: string,
						src: string,
						srcSet: string,
						srcSetWebp: string,
						srcWebp: string,
					}),
				}),
			]),
		}),
	]).isRequired,
	alt: string.isRequired,
}

export default Screen
