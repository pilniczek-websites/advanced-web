---
type: footer-fragment-left
title: "extended-info cs"
lang: cs
activeFrom: ""
activeTo: ""
---

**Sídlo:** Palachova&nbsp;1748/7, Ústí&nbsp;nad&nbsp;Labem, 400&nbsp;01

**IČO:** 02852128

Nejsem plátce DPH.
