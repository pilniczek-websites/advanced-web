import React from "react"
import { Field, ErrorMessage } from "formik"
import { bool, shape, string } from "prop-types"

const FormItem = ({ item, value, error, formId, cms, ...rest }) => {
	const defRegex = {
		email: "^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$",
		tel: "[+][0-9]{3} [0-9]{3} [0-9]{3} [0-9]{3}",
		ico: "[0-9]{3} [0-9]{3} [0-9]{2}",
	}
	const validationRule = RegExp(defRegex[item.validation] || item.validation)

	/*
	 * Validate function will only be executed on mounted fields.
	 * If a field is unmounted during the flow, it will not be validated.
	 * https://jaredpalmer.com/formik/docs/guides/validation
	 */
	const validate = val => {
		if (item.required && !val) {
			return item.requiredMessage
		}
		if (item.validation !== "" && !validationRule.test(val)) {
			return item.validationMessage
		}
		return null
	}
	return (
		<div className="form-item" {...rest}>
			<div className="form-item__inner-row">
				<label className="form-label" htmlFor={item.name}>
					{item.label}
				</label>
			</div>
			<div className="form-item__inner-row">
				{item.field === "select" ? (
					<Field
						className={`form-input ${
							typeof error === "undefined" ? "valid-input" : "invalid-input"
						}`}
						component={item.field}
						type={item.type}
						name={`${item.name}`}
						id={`${formId}-${item.name}`}
						placeholder={item.placeholder}
						validate={validate}
						value={value}
						error={error}
					>
						{item.options.map(option => (
							<option value={option.value} key={option.key}>
								{option.label}
							</option>
						))}
					</Field>
				) : (
					<Field
						className={`form-input ${
							typeof error === "undefined" ? "valid-input" : "invalid-input"
						}`}
						component={item.field}
						type={item.type}
						name={`${item.name}`}
						id={`${formId}-${item.name}`}
						placeholder={item.placeholder}
						validate={validate}
						value={value}
						error={error}
					/>
				)}

				{cms ? (
					<>
						<p className="form-field-error">{item.validationMessage}</p>
						<p className="form-field-error">{item.requiredMessage}</p>
					</>
				) : (
					<ErrorMessage
						name={item.name}
						component="p"
						className="form-field-error"
					/>
				)}
			</div>
		</div>
	)
}

FormItem.propTypes = {
	item: shape({
		name: string.isRequired,
		label: string.isRequired,
		field: string.isRequired,
		placeholder: string.isRequired,
		validation: string.isRequired,
		validationMessage: string.isRequired,
		required: bool.isRequired,
		requiredMessage: string.isRequired,
	}).isRequired,
	cms: bool,
	value: string,
	error: string,
	formId: string.isRequired,
}

FormItem.defaultProps = {
	cms: false,
	value: "",
	error: undefined,  
}

export default FormItem
