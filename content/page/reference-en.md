---
type: page
template: reference
title: Reference EN
metaTitle: "Reference"
metaDescription: I was part of these projects.
url: reference
lang: en
switchUrl: "reference"
inMenu: true
menuOrder: 3
menuTitle: "Reference"
isRedirected: false
isRedirectPermanent: false
redirectUrl: ""
index: "index"
follow: "follow"
activeFrom: ""
activeTo: ""
category: none
---

# Reference

I was part of these projects. (And in some cases I still am.)
