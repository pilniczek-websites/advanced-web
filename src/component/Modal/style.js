import styled from "styled-components"

const StyledModal = styled.div`
	.modal__headline {
		font-size: 30px;
		line-height: 30px;
		padding: 25px 40px;
	}
	.modal__close {
		font-size: 30px;
		line-height: 30px;
		width: 40px;
		padding: 5px;
		right: 15px;
		top: 15px;
		position: absolute;
	}
	.modal__content {
		padding: 50px 40px 0 40px;
		border-top: 1px solid rgba(0, 0, 0, 0.1);
		height: 100%;
	}
`

export default StyledModal
