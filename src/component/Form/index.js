import React from "react"
import { string, bool, objectOf, object } from "prop-types"
import { Form as FormikForm } from "formik"
import FormItem from "./FormItem"
import FormSubmit from "./FormSubmit"
import StyledForm from "./style"
// import FormReCaptcha from "./FormReCaptcha"

const Form = ({ isSubmitting, status, values, errors, touched, data, id, cms }) => {
	return (
		<StyledForm>
			<FormikForm>
				{data.fieldset.map((item) => (
					<div className="form__fieldset" key={item.id}>
						<p className="form__headline">{item.headline}</p>
						{item.input.map((subitem) => {
							return (
								<FormItem
									key={item.name}
									item={subitem}
									value={values[subitem.name]}
									error={touched[subitem.name] && errors[subitem.name]}
									formId={id}
									cms={cms}
								/>
							)
						})}
					</div>
				))}
				{/* reCaptchaVisible && (
					<div className="form__fieldset">
						<FormReCaptcha setFieldValue={setFieldValue} />
					</div>
				) */}
				<div className="form__fieldset" style={{ flexDirection: "column" }}>
					<FormSubmit
						isSubmitting={isSubmitting}
						text={data.submit}
						status={status}
						onClick={()=>{/*
							console.log("yes")
							console.log('log status: ', status)
							console.log('log errors: ', errors)
							console.log('log touched: ', touched)
						*/}}
					/>
				</div>
			</FormikForm>
		</StyledForm>
	)
}

Form.propTypes = {
	isSubmitting: bool.isRequired,
	status: string,
	values: objectOf(string),
	errors: objectOf(string),
	touched: objectOf(bool),
	data: object.isRequired, // eslint-disable-line
	id: string.isRequired,
	// setFieldValue: func.isRequired,
	// reCaptchaVisible: bool.isRequired,
	cms: bool.isRequired,
}

Form.defaultProps = {
	status: undefined,
	values: {},
	errors: {},
	touched: {},
}

export default Form
