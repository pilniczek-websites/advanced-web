import React from "react"
import styled from "styled-components"

const StyledSVG = styled(props => <svg {...props} />)``

export default StyledSVG
