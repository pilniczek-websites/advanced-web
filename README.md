# Advanced-web

<p style="display:flex">Master: <a style="margin:0 20px 0 5px" href="https://gitlab.com/pilniczek/advanced-web/commits/master"><img alt="pipeline status" src="https://gitlab.com/pilniczek/advanced-web/badges/master/pipeline.svg" /></a> Dev: <a style="margin:0 20px 0 5px" href="https://gitlab.com/pilniczek/advanced-web/commits/dev"><img alt="pipeline status" src="https://gitlab.com/pilniczek/advanced-web/badges/dev/pipeline.svg" /></a></p>

This is just my pet project. :)

## Tools

- https://nodejs.org/en/
- https://reactjs.org/
- https://www.gatsbyjs.org/
- https://www.netlifycms.org/ (todo)
- https://eslint.org/
- https://sentry.io/for/react/ (todo)
- https://git-scm.com/
- https://www.docker.com/
- https://docs.gitlab.com/ee/ci/
- https://docs.gitlab.com/ee/user/project/pages/

## Repo

### Push rule

- Feature or fix commit: `[f,F,x,X]:? #\d+ \S+.*`
- Merge commit: `[M,m]erge branch .*`
- Netlify CMS update content: `[U,u]pdate .*`
- Netlify CMS upload file: `[U,u]pload .*`

**Ccompleted regex:** `^([f,F,x,X]:? #\d+ \S+.*)|([M,m]erge branch .*)|([U,u]pdate .*)|([U,u]pload .*)`

### Branch name

- Feature branch: `\d+-\S+`
- Dev: `dev`
- Master: `master`

**Completed regex:** `(^\d+-\S+)|(dev)|(master)`

## Feedback

Message me! https://twitter.com/pilniczek

## Contribution

Currently unavailable.

## License

MIT

## Inspiration & resources

### Netlify

- [CMS](https://github.com/netlify/netlify-cms)
- [Example](https://cms-demo.netlify.com/#/collections/posts)
- [GitLab setup](https://docs.gitlab.com/ee/integration/oauth_provider.html#adding-an-application-through-the-profile)
- [Customize UI](https://github.com/netlify/netlify-cms/issues/2019#issuecomment-465313275)

### Design

- [break pointy](https://www.vzhurudolu.cz/prirucka/breakpointy)
- [easing functions](https://matthewlein.com/tools/ceaser)
- [flexbox tip: last item to right](https://medium.com/@iamryanyu/how-to-align-last-flex-item-to-right-73512e4e5912)

### Development

- [readme](https://www.makeareadme.com/)
- [gitignore.io](https://www.gitignore.io/)
- [conventional commits](https://www.conventionalcommits.org/en/v1.0.0-beta.3/)
- [licenses](https://choosealicense.com/licenses/)

---

### Notes

what about **react-helmet-async** instead of **https://github.com/nfl/react-helmet**?

### Tips

- [lifecycle to hook](https://dev.to/trentyang/replace-lifecycle-with-hooks-in-react-3d4n)
