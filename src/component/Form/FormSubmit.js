import React from "react"
import { shape, string, bool } from "prop-types"

const FormSubmit = ({ isSubmitting, text, status, cms, ...rest }) => {
	return (
		<>
			{isSubmitting || cms ? (
				<div className="form-item">
					<p className="form-submitting">{text.submitting}</p>
				</div>
			) : null}
			<div className="form-item">
				{status === "error" || cms ? (
					<p className="form-error">{text.error}</p>
				) : null}
				{status === "reCaptchaError" || cms ? (
					<p className="form-error">{text.reCaptchaError}</p>
				) : null}
				{status === "success" || cms ? (
					<p className="form-success">{text.success}</p>
				) : null}
			</div>
			<button type="submit" disabled={isSubmitting || cms} {...rest}>
				{text.label}
			</button>
		</>
	)
}

FormSubmit.propTypes = {
	text: shape({
		label: string.isRequired,
		submitting: string.isRequired,
		error: string.isRequired,
		reCaptchaError: string.isRequired,
		success: string.isRequired,
	}).isRequired,
	isSubmitting: bool.isRequired,
	status: string,
	cms: bool,
}

FormSubmit.defaultProps = {
	status: undefined,
	cms: false,
}

export default FormSubmit
