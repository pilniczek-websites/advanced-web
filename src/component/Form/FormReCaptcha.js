import React from "react"
import { func } from "prop-types"
import Recaptcha from "react-recaptcha"

const FormReCaptcha = ({ setFieldValue }) => {
	return (
		<div className="form-item">
			<Recaptcha
				sitekey="6LdOZdAUAAAAACdkLzd6ZsDaJDT5tvJwnC_JZ2RS"
				theme="dark"
				render="explicit"
				verifyCallback={response => {
					setFieldValue("recaptcha", response)
				}}
			/>
		</div>
	)
}

FormReCaptcha.propTypes = {
	setFieldValue: func.isRequired,
}

export default FormReCaptcha
