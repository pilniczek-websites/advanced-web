import React from "react"
import { string } from "prop-types"
import styled from "styled-components"

const StyledQuote = styled.blockquote`
	margin: 0 0 1rem;
	margin-bottom: 1rem;

	.blockquote__footer {
		display: block;
		font-size: 80%;
		color: #7a8288;
	}
	.blockquote__footer::before {
		content: "\2014\00A0";
	}
	@media print {
		blockquote {
			border: 1px solid #999;
			page-break-inside: avoid;
		}
		p {
			orphans: 3;
			widows: 3;
		}
	}
`

const Quote = ({ text, author }) => (
	<StyledQuote className="blockquote">
		<p>{text}</p>
		<div className="blockquote__footer">
			<cite title="Quote Author">{author}</cite>
		</div>
	</StyledQuote>
)

Quote.propTypes = {
	text: string.isRequired,
	author: string.isRequired,
}

export default Quote
